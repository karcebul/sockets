import * as bodyParser from 'body-parser';
import * as path from 'path';
import express from 'express';

export const app: express.Express = express();

const port = process.env.PORT || 3000;

app.use(bodyParser.json());
console.log(path.join(__dirname, '/../public'));
app.use(express.static(path.join(__dirname, '/../public')));

app.listen(port, () => {
  console.log(`Started on port ${port}`);
  app.emit('appStarted');
});
