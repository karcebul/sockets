"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var bodyParser = __importStar(require("body-parser"));
var path = __importStar(require("path"));
var express_1 = __importDefault(require("express"));
exports.app = express_1.default();
var port = process.env.PORT || 3000;
exports.app.use(bodyParser.json());
console.log(path.join(__dirname, '/../public'));
exports.app.use(express_1.default.static(path.join(__dirname, '/../public')));
exports.app.listen(port, function () {
    console.log("Started on port " + port);
    exports.app.emit('appStarted');
});
//# sourceMappingURL=server.js.map